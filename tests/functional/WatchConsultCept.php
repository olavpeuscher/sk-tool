<?php 
$I = new FunctionalTester($scenario);

// This tests checks the complete structure

$I->am('a SK tool user');
$I->wantTo('Sign in to my account and watch a consult');

$I->amOnPage('/');
$I->fillField('email', 'foo@bar.com');
$I->fillField('password', 'test');
$I->click('Submit');

$I->seeInCurrentUrl('/projects');

$I->click('Testproject');

$I->amOnPage('/projects/1');

$I->click('Eerste test');

$I->amOnPage('/tests/1');

$I->click('testconsult');

$I->amOnPage('/consults/1');

$I->click('Logout (testaccount)');

$I->amOnPage('/');

