<?php 
$I = new FunctionalTester($scenario);
$I->am('a sk tool user');
$I->wantTo('Sign in to my account and sign out');

$I->amOnPage('/login');
$I->fillField('email', 'foo@bar.com');
$I->fillField('password', 'test');
$I->click('Submit');

$I->seeInCurrentUrl('/projects');

$I->click('Logout (testaccount)');



