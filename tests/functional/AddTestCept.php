<?php 
$I = new FunctionalTester($scenario);

$I->am('a SK tool user');
$I->wantTo('Sign in to my account and create a new test within a project');

$I->amOnPage('/login');
$I->fillField('email', 'foo@bar.com');
$I->fillField('password', 'test');
$I->click('Submit');

$I->seeInCurrentUrl('/projects');

$I->click('Testproject');

$I->amOnPage('/projects/1');

$I->click('Nieuwe test');

//Images for samples will be added later on.

$I->fillField('name', 'New test');
$I->fillField('mod_name', 'Test moderator');
$I->fillField('product', 'Some product');

$I->click('Submit');

//Project id is chosen from current project

$I->seeRecord('tests', [
    'project_id'=> '1',
    'name'=> 'New test',
    'mod_name'=> 'Test moderator',
    'product'=> 'Some product'
]);

$I->seeCurrentUrlEquals('/projects/1');

