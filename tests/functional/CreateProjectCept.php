<?php 
$I = new FunctionalTester($scenario);

$I->am('a SK tool user');
$I->wantTo('Sign in to my account and create a new project');

$I->amOnPage('/login');
$I->fillField('email', 'foo@bar.com');
$I->fillField('password', 'test');
$I->click('Submit');

$I->seeInCurrentUrl('/projects');

$I->click('New project');

$I->amOnPage('/projects/create');

$I->selectOption('company_id', 'Unilever');
$I->fillField('name', 'Testproject');
$I->fillField('brand', 'TestBrand');
$I->fillField('number', 'P00001');
$I->fillField('date', '2015-04-04');

$I->click('Submit');

$I->seeRecord('users', [
    'company_id'=> '1',
    'name'=> 'Testproject',
    'brand'=> 'TestBrand',
    'number'=> 'P00001',
    'date'=> '2015-04-04'
]);

$I->seeCurrentUrlEquals('/projects/');

