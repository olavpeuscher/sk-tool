# SK Ranking tool

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)

This is a project developed for the Smurfit Kappa Design Centre (SK-DC) while being a student at the University of Twente. The tool features a ranking tool for new products.

The used framework is Laravel with some dependencies. Laravel is a web application framework with expressive, elegant syntax. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

## Requirements

* Composer
* PHP 5.5
* Mysql

## Installation

The SK tool has several dependencies, these will all be added while running coposer install.

* Install PHP 5.5 (make sure php -v says your specific version) 5.4 isn't supported
* Install composer
* Install the mcrypt extension
* Start a local mysql server on port 3306 with a root user with no password, and create a database sk-tool
* run composer install
* run php artisan migrate --seed

## Environments

Laravel will detect if you are using a development machine through 'bootstrap/start.php'. All of the development config files will be loaded from 'app/config/dev'. And will overwrite the default production values.

Start the dev environment with:

```bash
$ php artisan --env='dev' serve
```

## Testing

Testing is done with Codeception. Functional tests are added an can be run with:

```bash
$ cd sk-tool
$ vendor/bin/codecept run functional
```

If codecept is not set as an executable yet, change the permissions:

```bash
$ sudo chmod a+x vendor/bin/codecept
```