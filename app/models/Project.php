<?php

class Project extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('company_id', 'name', 'brand', 'number', 'date');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';


    /**
     * Relations within this model
    **/
	public function users()
    {
        return $this->belongsToMany('User', 'user_projects');
    }

    public function tests()
    {
        return $this->hasMany('Test');
    }

}
