<?php

class UserProjects extends Eloquent {

    /**
     * The field which may be mass assigned
     *
     * @var array
     */
    protected $fillable = array('project_id', 'user_id');

}
