<?php

class Test extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('project_id', 'name', 'mod_name', 'product');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tests';


    /**
     * Relations within this model
    **/
	public function project()
    {
        return $this->belongsTo('Project');
    }

    public function consults()
    {
        return $this->hasMany('Consult');
    }

}
