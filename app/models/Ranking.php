<?php

class Ranking extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('consult_id', 'sample_id', 'type', 'value');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rankings';

	public function consult()
    {
        return $this->belongsTo('Consult');
    }
}
