<?php

class Consult extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('test_id', 'name', 'mod_name', 'comment', 'comment_brand', 'comment_targetgroup');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'consults';

	/**
     * Relations within this model
    **/
	public function test()
    {
        return $this->belongsTo('Test');
    }

    public function rankings()
    {
        return $this->hasMany('Ranking');
    }
}
