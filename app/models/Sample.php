<?php

class Sample extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('image_id', 'test_id', 'name');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'samples';


}
