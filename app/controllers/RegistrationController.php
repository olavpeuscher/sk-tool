<?php

use Tool\Forms\RegistrationForm;

class RegistrationController extends \BaseController {

	/**
	* @var RegistrationForm
	*/
	private $registrationForm;

	function __construct(Tool\Forms\RegistrationForm $registrationForm)
	{
		$this->registrationForm = $registrationForm;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('registration.create');
	}

	/**
	 * Create a new user.
	 *
	 * @return String
	 */
	public function store()
	{
		$formData = Input::all();

		$this->registrationForm->validate($formData);

		$user = User::create(
			[
				'username' => Input::get('username'),
				'email' => Input::get('email'),
				'password' => Hash::make(Input::get('password'))
				]
		);

		Auth::login($user);

		return Redirect::home();
	}

	/**
	* Registration must be validated by a mod in the future
	**/

}