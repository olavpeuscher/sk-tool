<?php

class CompaniesController extends \BaseController {

    public function index()
    {
        $companies = Company::all();

        return View::make('pages.companies.index', [ 'companies' => $companies ]); 
    }
}
