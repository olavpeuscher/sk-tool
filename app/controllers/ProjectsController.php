<?php

use Tool\Forms\RegistrationForm;
use Laracasts\Validation\FormValidationException;

class ProjectsController extends \BaseController {

	/**
	* @var ProjectsForm
	*/
	private $projectsForm;

	function __construct(Tool\Forms\ProjectsForm $projectsForm)
	{
		$this->projectsForm = $projectsForm;

		$this->beforeFilter('auth');
	}

	/**
	 *
	 * @return Response
	 */
	public function index()
	{
		//TODO Permission so another user cannot view the project

		$projects = Auth::user()->projects;
		
		return View::make('pages.projects.index', ['projects' => $projects ]);
	}

	/**
	 *
	 * @return Response
	 */
	public function show($project_id)
	{		
		$project = Project::whereId($project_id)->first();

		$tests = $project->tests;

		return View::make('pages.projects.show', [ 'project' => $project , 'tests' => $tests ]);
	}

	/**
	 * Show the form for creating a new project.
	 *
	 * @return Response
	 */
	public function create()
	{

		// queries the companies table, and orders it asc
  		$company_options = DB::table('companies')->orderBy('name', 'asc')->lists('name','id');

		return View::make('pages.projects.create', [ 'company_options' => $company_options ]);
	}

	/**
	 * Create a new project.
	 *
	 * @return String
	 */
	public function store()
	{
		$formData = Input::all();

		$this->projectsForm->validate($formData);

		$project = Project::create(
			[
				'company_id' => Input::get('company_id'),
				'name' => Input::get('name'),
				'brand' => Input::get('brand'),
				'number' => Input::get('number'),
				'date' => Input::get('date')
			]
		);

		// Get the current user_id and the id of the created project

		$project_id = $project->id;

		$user_id = Auth::id();

		$userProject = UserProjects::create(
			[
				'user_id' => $user_id,
				'project_id' => $project_id
			]
		);

		return Redirect::to('/projects');
	}

}
