<?php

use Tool\Forms\SignInForm;

class SessionsController extends \BaseController {

	/**
	* @var SignInForm
	*/
	private $signInForm;

	function __construct(Tool\Forms\SignInForm $signInForm)
	{
		$this->signInForm = $signInForm;

		$this->beforeFilter('guest');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sessions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$formData = Input::only('email', 'password');
		
		// Validate the form

		$this->signInForm->validate($formData);
		
		// If invalid go back
		if (Auth::attempt($formData))
		{
			// if valid, sign in and show projects page
			return Redirect::intended('/projects');
		}

		return Redirect::back()->withInput()->withFlashMessage('Gegevens zijn incorrect.');
		
	}
    
}