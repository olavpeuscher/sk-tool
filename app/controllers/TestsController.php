<?php

class TestsController extends \BaseController {

    public function show($test_id)
    {

        $test = Test::find($test_id);

        $project = $test->project;

        $consults = $test->consults;

        return View::make('pages.tests.show',
            [ 
                'consults' => $consults,
                'test' => $test,
                'project' => $project 
            ]
        );
    }
}
