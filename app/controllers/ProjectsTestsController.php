<?php

use Tool\Forms\TestsForm;

class ProjectsTestsController extends \BaseController {

    /**
    * @var ProjectsForm
    */
    private $testsForm;

    function __construct(Tool\Forms\testsForm $testsForm)
    {
        $this->testsForm = $testsForm;

        $this->beforeFilter('auth');
    }

    /**
     * Show the form for creating a new project.
     *
     * @return Response
     */
    public function create($project_id)
    {
        return View::make('pages.tests.create',[ 'project_id' => $project_id ]);
    }

    
    /**
     * Create a new project.
     *
     * @return String
     */
     public function store($project_id)
    {

        $formData = Input::all();

        $this->testsForm->validate($formData);

        $test = Test::create(
            [
                'project_id' => $project_id,
                'name' => Input::get('name'),
                'product' => Input::get('product'),
                'mod_name' => Input::get('mod_name')
            ]
        );

        return Redirect::action('ProjectsController@show', [ 'project_id' => $project_id ]);

    }
} 