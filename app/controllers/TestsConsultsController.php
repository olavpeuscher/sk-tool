<?php

use Tool\Forms\ConsultsForm;

class TestsConsultsController extends \BaseController {

    /**
    * @var ProjectsForm
    */
    private $consultsForm;

    function __construct(Tool\Forms\consultsForm $consultsForm)
    {
        $this->consultsForm = $consultsForm;

        $this->beforeFilter('auth');
    }

    /**
     * Show the form for creating a new project.
     *
     * @return Response
     */
    public function create($test_id)
    {
        return View::make('pages.consults.create',[ 'test_id' => $test_id ]);
    }

    
    /**
     * Create a new project.
     *
     * @return String
     */
     public function store($test_id)
    {

        $formData = Input::all();

        $this->consultsForm->validate($formData);

        $consult = Consult::create(
            [
                'test_id' => $test_id,
                'name' => Input::get('name'),
                'mod_name' => Input::get('mod_name')
            ]
        );

        return Redirect::action('TestsController@show', [ 'test_id' => $test_id ]);

    }
} 