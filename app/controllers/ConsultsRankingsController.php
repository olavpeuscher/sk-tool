<?php

use Tool\Forms\RankingsForm;

class ConsultsRankingsController extends \BaseController {

    /**
    * @var ProjectsForm
    */
    private $rankingsForm;

    function __construct(Tool\Forms\rankingsForm $rankingsForm)
    {
        $this->rankingsForm = $rankingsForm;

        $this->beforeFilter('auth');
    }

    /**
     * Show the form for creating a new project.
     *
     * @return Response
     */
    public function create($consult_id)
    {
        return View::make('pages.rankings.create',[ 'consult_id' => $consult_id ]);
    }

    
    /**
     * Create a new project.
     *
     * @return String
     */
     public function store($consult_id)
    {

        $formData = Input::all();

        $this->rankingsForm->validate($formData);

        $ranking = Ranking::create(
            [
                'consult_id' => $consult_id,
                'value' => Input::get('value'),
                'type' => Input::get('type')
            ]
        );

        return Redirect::action('ConsultsController@show', [ 'consult_id' => $consult_id ]);

    }
} 