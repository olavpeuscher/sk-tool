<?php

class ConsultsController extends \BaseController {

    public function show($consult_id)
    {
        $consult = Test::find($consult_id);

        return View::make('pages.consults.show', [ 'consult' => $consult ]);
    }
}
