<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	
	<title>SK Development</title>

	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/main.css">

	<script type="text/javascript" src="/js/lib/jquery-1.10.2.js"></script>
  	<script type="text/javascript" src="/js/lib/jquery-ui-1.11.1.js"></script>

  	<script type="text/javascript" src="/js/ui.js"></script>

</head>
<body>

	<div class="wrapper">

		@include('layouts.partials.nav')
		
		@yield('content')

	</div>
	
</body>
</html>