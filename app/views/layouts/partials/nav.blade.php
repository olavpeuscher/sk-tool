<header class="navbar navbar-static-top bg-dark" role="banner">
    <div class="wrapper">
        <div class="navbar-header sk-logo pull-left"></div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
           
            @if(Auth::check())

                <ul class="nav navbar-nav">
                    <li>{{ link_to_route('projects_path', 'Projecten', null, null) }}</li>

                    <li>{{ link_to_route('companies_path', 'Bedrijven', null, null) }}</li>

                    <li>{{ link_to_route('projects_path', 'Gebruikers', null, null) }}</li>
                </ul>
            @endif

            <ul class="nav navbar-nav navbar-right">
            
            @if(Auth::check())
                <li>
                    {{ link_to_route('logout_path', 'Logout ('.Auth::user()->username.')') }}
                </li>
            @else
                <li>{{ link_to_route('register_path', 'Sign up', null, null) }}</li>
                <li>{{ link_to_route('login_path', 'Sign in', null, null) }}</li>
            @endif
            </ul>
        </nav>
    </div>
</header>