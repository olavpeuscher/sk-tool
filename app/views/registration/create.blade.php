@extends ('layouts.default')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-xs-12 page-title">
				<h3>Register</h3>
			</div>

			<div class="row">
            	<div class="col-xs-6">

					@if($errors->any())
						
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error}}</li>
								@endforeach
							</ul>
						</div>

					@endif
				</div>
			</div>

			<div class="row">
            	<div class="col-xs-6">

				{{ Form::open(['route' => 'register_path']) }}

					<!-- Username form input -->
					<div class="form-group input">
						{{ Form::label('username', 'Username:') }}
						{{ Form::text('username', null, ['class' => 'form-control']) }}
					</div>

					<!-- Email form input -->
					<div class="form-group input">
						{{ Form::label('email', 'Email:') }}
						{{ Form::text('email', null, ['class' => 'form-control']) }}
					</div>

					<!-- Password form input -->
					<div class="form-group input">
						{{ Form::label('password', 'Password:') }}
						{{ Form::password('password', ['class' => 'form-control']) }}
					</div>

					<div class="form-group input">
						{{ Form::label('password_confirmation', 'Password confirmation:') }}
						{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
					</div>

					<!-- Submit buttom -->
					<div class="form-group">
						{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
					</div>

				{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>

@stop