@extends('layouts.default')

@section('content')

	<div class="bg-image"></div>
	
	<div class="container">

		<div class="row">
            <div class="col-xs-6 col-xs-offset-3">

				@if($errors->any())
				
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error}}</li>
						@endforeach
					</ul>
				</div>

				@endif
			</div>
		</div>

		<div class="row">

            <div class="col-xs-6 col-xs-offset-3">

            	<div class="login">
            		<h3>Log in</h3>

					{{ Form::open(['route' => 'login_path']) }}

					<!-- Email form input -->
						<div class="form-group input">
							{{ Form::label('email', 'Email:') }}
							{{ Form::text('email', null, ['class' => 'form-control']) }}
						</div>

						<!-- Password form input -->
						<div class="form-group input">
							{{ Form::label('password', 'Password:') }}
							{{ Form::password('password', ['class' => 'form-control']) }}
						</div>

						<!-- Submit buttom -->
						<div class="form-group">
							{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
						</div>
					
					{{ Form::close() }}
				</div>

			</div>
		</div>
	</div>
@stop

