@extends('layouts.default')

@section('content')
    <div class="container">

        <h3>Bedrijven</h3>

        <ul>
        @foreach ($companies as $company)
            <li>{{ $company }}</li>
        @endforeach
        </ul>
        
    </div>
@stop