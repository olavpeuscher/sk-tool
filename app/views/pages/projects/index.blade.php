@extends('layouts.default')

@section('content')
    <div class="container">

         <div class="row">
            <div class="col-xs-12 page-title">
                <h3>Mijn projecten</h3>
            </div>
        </div>

         <div class="row">
            <div class="col-xs-12">
                 {{ link_to_route('project_create_path', 'New project', null, ['class' => 'btn btn-default']) }}
            </div>
        </div>
        
        <div>
            <ul class="fluid-list col-xs-12">

                <li class="row heading">
                    <div class="col-xs-2">Project nummer</div>
                    <div class="col-xs-4">Project naam</div>
                    <div class="col-xs-4">Project Merk</div>
                    <div class="col-xs-2">Startdatum</div>
                </li>
            
                @foreach ($projects as $project)
                
                    <a href="{{ URL::route('project_show_path', $project->id) }}">
                        <li class="row">
                            <div class="col-xs-2">{{ $project->number }}</div>
                            <div class="col-xs-4">{{ $project->name }}</div>
                            <div class="col-xs-4">{{ $project->brand }}</div>
                            <div class="col-xs-2">{{ $project->date }}</div>
                        </li>
                    </a>

                @endforeach
            </ul>
        </div>

    </div>
@stop



