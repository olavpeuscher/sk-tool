@extends('layouts.default')

@section('content')
     <div class="container">

         <div class="row">
            <div class="col-xs-12 page-title">
                <div class="project-number pull-left">{{ $project->number }}</div>
                <h3>{{ $project->name }}</h3>

                 <div class="project-details pull-right">Merk: {{ $project->brand }} | Startdatum: {{ $project->date }}</div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                {{ link_to_route('test_create_path', 'Nieuwe test', ['project_id' => $project->id], ['class' => 'btn btn-default']) }}

            </div>
        </div>

        <div>

            @if (count($tests) >= 1)

            <ul class="fluid-list col-xs-12">

                 <li class="row heading">
                    <div class="col-xs-4">Test naam</div>
                    <div class="col-xs-4">Test product</div>
                    <div class="col-xs-2">Invoerdatum</div>
                </li>
            
                @foreach ($tests as $test)
                
                    <a href="{{ URL::route('test_show_path', $test->id) }}">
                        <li class="row">
                            <div class="col-xs-4">{{ $test->name }}</div>
                            <div class="col-xs-4">{{ $test->product }}</div>
                            <div class="col-xs-4">{{ $test->created_at }}</div>
                        </li>
                    </a>

                @endforeach
            </ul>
            
            @else
            
                <P>Er zijn geen tests.</P>

            @endif
        </div>

    </div>
@stop

