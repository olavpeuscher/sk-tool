@extends('layouts.default')

@section('content')
	
	<div class="container">
		 
		 <div class="row">
	        <div class="col-xs-12 page-title">
	            <h3>Nieuw project</h3>
	        </div>
	    </div>
	    
	    <div class="row">
	    	<div class="col-xs-6">
	    		@if($errors->any())
						
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error}}</li>
							@endforeach
						</ul>
					</div>

				@endif
			</div>
		</div>

		 <div class="row">
	    	<div class="col-xs-6">

				{{ Form::open(['route' => 'project_create_path']) }}

					<!-- Project name form input -->
					<div class="form-group input">
						{{ Form::label('company_id', 'Company:') }}
						{{ Form::select('company_id', $company_options , null, ['class' => 'form-control']) }}
					</div>

					<!-- Project name form input -->
					<div class="form-group input">
						{{ Form::label('name', 'Project name:') }}
						{{ Form::text('name', null, ['class' => 'form-control']) }}
					</div>

					<!-- project brand form input -->
					<div class="form-group input">
						{{ Form::label('brand', 'Project brand:') }}
						{{ Form::text('brand', null, ['class' => 'form-control']) }}
					</div>

					<!-- project number form input -->
					<div class="form-group input">
						{{ Form::label('number', 'Project number:') }}
						{{ Form::text('number', null, ['class' => 'form-control']) }}
					</div>

					<!-- project start date form input -->
					<div class="form-group input">
						{{ Form::label('date', 'Project start date:') }}
						{{ Form::input('date', 'date', '', ['class' => 'form-control']) }}
					</div>

					
					<!-- Submit buttom -->
					<div class="form-group">
						{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
					</div>
				
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop