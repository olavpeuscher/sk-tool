@extends('layouts.default')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-xs-12">
            
             <!--Moving images as a test-->
             <div class="ranking-input">
                 <div class="containment-wrapper">
                    <div class="draggable draggable-image ui-widget-content">
                        <img src="http://lorempixel.com/170/130/nightlife/" />
                    </div>
                    <div class="draggable draggable-image ui-widget-content">
                        <img src="http://lorempixel.com/170/130/nature/" />
                    </div>
                    <div class="draggable draggable-image ui-widget-content">
                        <img src="http://lorempixel.com/170/130/people/" />
                    </div>
                    <div class="draggable draggable-image ui-widget-content">
                        <img src="http://lorempixel.com/170/130/sports/" />
                    </div>
                </div>
                <div>
                    <p>Brand fit</p>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
             {{ link_to_route('projects_path', 'Volgende stap', null, ['class' => 'btn btn-default pull-right']) }}
        </div>
    </div>
</div>
@stop