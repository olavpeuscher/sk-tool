@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 page-title">
                <h2>Nieuwe test</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">

                @if($errors->any())
                        
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif

            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">

                {{ Form::open(['route' => ['test_create_path', $project_id]]) }}

                    <!-- Test name form input -->
                    <div class="form-group input">
                        {{ Form::label('name', 'Test name:') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>
                    
                    <!-- Test mod name form input -->
                    <div class="form-group input">
                        {{ Form::label('name', 'Moderator name:') }}
                        {{ Form::text('mod_name', null, ['class' => 'form-control']) }}
                    </div>

                     <!-- Test mod name form input -->
                    <div class="form-group input">
                        {{ Form::label('product', 'Product name:') }}
                        {{ Form::text('product', null, ['class' => 'form-control']) }}
                    </div>

                    <!-- Submit buttom -->
                    <div class="form-group">
                        {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                    </div>
                
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
