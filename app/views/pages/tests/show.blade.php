@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 page-title">
                <div class="project-number pull-left">{{ $project->number }}</div>
                <h3>{{ $project->name }}</h3>

                <div class="project-details pull-right">Merk: {{ $project->brand }} | Startdatum: {{ $project->date }}</div>
            </div>
             <div class="col-xs-12 page-title">
                <h3>{{ $test->name }}</h3>

                <div class="project-details pull-right">Product: {{ $test->product }} | Project moderator: {{ $test->mod_name }}</div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                 {{ link_to_route('consult_create_path', 'Nieuw consult', ['test_id' => $test->id], ['class' => 'btn btn-default']) }}
            </div>
        </div>

        <div>

        @if (count($consults) >= 1)
        
            <ul class="fluid-list col-xs-12">

                 <li class="row heading">
                    <div class="col-xs-4">Consult naam</div>
                    <div class="col-xs-4">Consult moderator</div>
                    <div class="col-xs-2">Invoerdatum</div>
                </li>

                @foreach ($consults as $consult)

                 <a href="{{ URL::route('consult_show_path', $consult->id) }}">
                    <li class="row">
                        <div class="col-xs-4">{{ $consult->name }}</div>
                        <div class="col-xs-4">{{ $consult->mod_name }}</div>
                        <div class="col-xs-4">{{ $consult->created_at }}</div>
                    </li>
                </a>
                @endforeach
            </ul>

        @else
            <p>Er zijn geen consults.</p>
        @endif

    </div>

@stop