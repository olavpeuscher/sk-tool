<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
* Route the homepage
*/
Route::get('/', [ 'as' => 'home','uses' => 'SessionsController@create' ]);


/*
* Route to companies
*/
Route::get('/companies', [ 'as' => 'companies_path', 'uses' => 'CompaniesController@index' ]);

/*
* Route to projects
*/
Route::get('/projects', [ 'as' => 'projects_path', 'uses' => 'ProjectsController@index' ]);

Route::get('/projects/create', [ 'as' => 'project_create_path', 'uses' => 'ProjectsController@create' ]);

Route::post('/projects/create', [ 'as' => 'project_create_path', 'uses' => 'ProjectsController@store' ]);

/*
* Route to a project
*/
Route::get('/projects/{project_id}', [ 'as' => 'project_show_path', 'uses' => 'ProjectsController@show' ]);

/*
* Route to a test
*/
Route::get('/tests/{test_id}', [ 'as' => 'test_show_path', 'uses' => 'TestsController@show' ]);

Route::get('/projects/{project_id}/tests/create', [ 'as' => 'test_create_path', 'uses' => 'ProjectsTestsController@create' ]);

Route::post('/projects/{project_id}/tests/create', [ 'as' => 'test_create_path', 'uses' => 'ProjectsTestsController@store' ]);

/*
* Route to consults
*/
Route::get('/consults/{consult_id}', [ 'as' => 'consult_show_path', 'uses' => 'ConsultsController@show' ]);

Route::get('/tests/{test_id}/consults/create', ['as' => 'consult_create_path', 'uses' => 'TestsConsultsController@create']);

Route::post('/tests/{test_id}/consults/create', ['as' => 'consult_create_path', 'uses' => 'TestsConsultsController@store']);


/*
* Route to rankings
*/
Route::get('/rankings/{rankings_id}', [ 'as' => 'ranking_show_path', 'uses' => 'RankingsController@show' ]);

Route::get('/consults/{consult_id}/rankings/create', ['as' => 'ranking_create_path', 'uses' => 'ConsultsRankingsController@create']);

Route::post('/consults/{consult_id}/rankings/create', ['as' => 'ranking_create_path', 'uses' => 'ConsultsRankingsController@store']);

/*
* Register a new user
*/
Route::get('/register', [ 'as' => 'register_path', 'uses' => 'RegistrationController@create' ]);

Route::post('/register',[ 'as' => 'register_path', 'uses' => 'RegistrationController@store' ]);

/*
* Sessions
*/
Route::get('login', [ 'as' => 'login_path', 'uses' => 'SessionsController@create' ]);

Route::post('login', [ 'as' => 'login_path', 'uses' => 'SessionsController@store' ]);

Route::get('logout', array('as' => 'logout_path', function () {
    Auth::logout();
    return Redirect::route('home');
	}))->before('auth');
