<?php namespace Tool\Forms;

use Laracasts\Validation\FormValidator;

class RankingsForm extends FormValidator {

    /**
    * Validation rules for new project form
    * @var array
    */

    protected $rules = [
        'value' => 'required'
    ];
}