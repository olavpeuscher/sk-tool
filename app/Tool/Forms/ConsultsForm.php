<?php namespace Tool\Forms;

use Laracasts\Validation\FormValidator;

class ConsultsForm extends FormValidator {

    /**
    * Validation rules for new project form
    * @var array
    */

    protected $rules = [
        'name' => 'required',
        'mod_name' => 'required',
    ];
}