<?php namespace Tool\Forms;

use Laracasts\Validation\FormValidator;

class ProjectsForm extends FormValidator {

	/**
	* Validation rules for new project form
	* @var array
	*/

	protected $rules = [
		'company_id' => 'required',
		'name' => 'required|unique:projects',
		'brand' => 'required',
		'number' => 'required',
		'date' => ''
	];
}