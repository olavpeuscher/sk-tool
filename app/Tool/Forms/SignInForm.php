<?php namespace Tool\Forms;

use Laracasts\Validation\FormValidator;

class SignInForm extends FormValidator {

	/**
	* Validation rules for sign in form
	* @var array
	*/

	protected $rules = [
		'email' => 'required',
		'password' => 'required'
	];
}