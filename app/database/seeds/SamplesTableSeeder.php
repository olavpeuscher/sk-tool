<?php

class SamplesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    	Sample::create(
    		array(
    			'image_id' => '1',
    			'name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	Sample::create(
    		array(
    			'image_id' => '2',
    			'name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	Sample::create(
    		array(
    			'image_id' => '3',
    			'name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	Sample::create(
    		array(
    			'image_id' => '4',
    			'name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	
	}

}
