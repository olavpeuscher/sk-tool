<?php

class ProjectsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Project::create(array(
        	'company_id' => '1',
        	'name' => 'Testproject',
        	'brand' => 'Knorr',
        	'number' => 'P0001A',
        	'date' => '2015-02-01'
        	)
        );
        Project::create(array(
        	'company_id' => '2',
        	'name' => 'Testproject2',
        	'brand' => 'Bolletje',
        	'number' => 'P0002A',
        	'date' => '2015-03-02'
        	)
        );
        Project::create(array(
                'company_id' => '3',
                'name' => 'Testproject3',
                'brand' => 'De Ruijter',
                'number' => 'P0003A',
                'date' => '2015-03-02'
                )
        );
	}

}
