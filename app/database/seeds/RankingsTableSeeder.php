<?php

class RankingsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '1',
    			'type' => '1',
    			'value' => '200'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '2',
    			'type' => '1',
    			'value' => '400'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '3',
    			'type' => '1',
    			'value' => '600'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '4',
    			'type' => '1',
    			'value' => '800'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '1',
    			'type' => '2',
    			'value' => '250'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '2',
    			'type' => '2',
    			'value' => '450'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '3',
    			'type' => '2',
    			'value' => '650'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '4',
    			'type' => '2',
    			'value' => '850'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '1',
    			'type' => '3',
    			'value' => '150'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '2',
    			'type' => '3',
    			'value' => '350'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '3',
    			'type' => '3',
    			'value' => '550'
    		)
    	);
    	Ranking::create(
    		array(
    			'consult_id' => '1',
    			'sample_id' => '4',
    			'type' => '3',
    			'value' => '950'
    		)
    	);
	}

}
