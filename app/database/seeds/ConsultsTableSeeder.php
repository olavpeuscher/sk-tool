<?php

class ConsultsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Consult::create(array(
        	'test_id' => '1',
        	'name' => 'testconsult',
        	'mod_name' => 'Moderator',
        	'comment' => 'Consult comment',
        	'comment_brand' => 'Consult comment brand',
        	'comment_targetgroup' => 'Consult comment target group',
        	)
        );
	}

}
