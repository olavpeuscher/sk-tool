<?php

class TestsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Test::create(array(
        	'project_id' => '1',
        	'name' => 'Eerste test',
        	'mod_name' => 'Moderator Test',
        	'product' => 'Knorr Gehaktballetjes'
        	)
        );
        Test::create(array(
        	'project_id' => '1',
        	'name' => 'Tweede test',
        	'mod_name' => 'Moderator Test',
        	'product' => 'Knorr Gehaktballetjes tweede test'
        	)
        );
	}

}
