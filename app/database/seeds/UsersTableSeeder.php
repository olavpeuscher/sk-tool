<?php

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        User::create(array(
        	'email' => 'foo@bar.com',
        	'username' => 'testaccount',
        	'password' => Hash::make('test'),
        	'remember_token' => Hash::make('token'),
        	)
        );
	}

}
