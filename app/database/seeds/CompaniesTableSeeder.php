<?php

class CompaniesTableSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Company::create(array(
            'name' => 'Unilever'
            )
        );
        Company::create(array(
            'name' => 'Bolletje N.V.'
            )
        );
        Company::create(array(
            'name' => 'De Ruijter'
            )
        );
    }


}
