<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consults', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('test_id');
			$table->string('name');
			$table->string('mod_name');
			$table->string('comment')->nullable();
			$table->string('comment_brand')->nullable();
			$table->string('comment_targetgroup')->nullable();

			$table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade'); // Relation with test_id

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consults');
	}

}
