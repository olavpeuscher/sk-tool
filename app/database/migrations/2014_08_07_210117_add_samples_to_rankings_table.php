<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSamplesToRankingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rankings', function($table)
		{
			$table->unsignedInteger('sample_id');
			$table->foreign('sample_id')->references('id')->on('samples')->onDelete('cascade'); // Relation with sample_id
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rankings', function($table)
		{
			$table->dropForeign('rankings_sample_id_foreign');
			$table->dropColumn(['sample_id']);
		});
	}

}
