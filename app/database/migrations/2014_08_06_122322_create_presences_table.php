<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('presences', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('consult_id');
			$table->string('name');
			$table->tinyInteger('function');

			$table->foreign('consult_id')->references('id')->on('consults')->onDelete('cascade'); // Relation with consult_id

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('presences');
	}

}
