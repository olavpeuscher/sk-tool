<?php

use Tool\Forms\RegistrationForm;

class ProjectsController extends \BaseController {

	/**
	* @var ProjectsForm
	*/
	private $projectsForm;

	function __construct(Tool\Forms\ProjectsForm $projectsForm)
	{
		$this->projectsForm = $projectsForm;

		$this->beforeFilter('auth');
	}

	/**
	 * Show the form for creating a new project.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('pages.projects.create');
	}

	/**
	 * Create a new project.
	 *
	 * @return String
	 */
	public function store()
	{
		$formData = Input::all();

		$this->projectsForm->validate($formData);

		$project = Project::create(array(
			'company_id' => Input::get('company_id'),
			'project_name' => Input::get('project_name'),
			'project_brand' => Input::get('project_brand'),
			'project_number' => Input::get('project_number'),
			'project_date' => Input::get('project_date')
		));

		return Redirect::to('/projects');
	}

	/**
	 * Show a list of project TODO: make the list dependent of the user
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = Auth::user()->projects;
		
		return View::make('pages.projects.index')->with('projects', $projects);
	}

	public function show($id)
	{
			$project = Project::findOrFail($id);
		
			return View::make('pages.projects.show')->with('project' ,$project->project_name);
	}
}
