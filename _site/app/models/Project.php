<?php

class Project extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('company_id', 'project_name', 'project_brand', 'project_number', 'project_date');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';


	public function users()
    {
        return $this->belongsToMany('Users', 'users_projects');
    }

    public function tests()
    {
        return $this->hasMany('Tests');
    }

}
