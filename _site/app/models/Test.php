<?php

class Test extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('project_id', 'test_name', 'test_mod_name', 'test_product');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tests';

	public function project()
    {
        return $this->belongsTo('Project');
    }

}
