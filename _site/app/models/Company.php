<?php

class Company extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = 'company_name';
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'companies';


}
