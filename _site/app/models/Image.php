<?php

class Image extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('image_url');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'images';


}
