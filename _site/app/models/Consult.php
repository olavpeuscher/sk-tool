<?php

class Consult extends Eloquent {

	/**
	 * The field which may be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array('test_id', 'consult_name', 'consult_mod_name', 'consult_comment', 'consult_comment_brand', 'consult_comment_targetgroup');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'consults';


}
