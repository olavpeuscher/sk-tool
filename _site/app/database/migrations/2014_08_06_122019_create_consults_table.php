<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consults', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('test_id');
			$table->string('consult_name');
			$table->string('consult_mod_name');
			$table->string('consult_comment');
			$table->string('consult_comment_brand');
			$table->string('consult_comment_targetgroup');

			$table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade'); // Relation with test_id

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consults');
	}

}
