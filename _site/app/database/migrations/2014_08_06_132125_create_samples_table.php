<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSamplesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('samples', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('image_id');
			$table->unsignedInteger('test_id');
			$table->string('sample_name');

			$table->foreign('image_id')->references('id')->on('images')->onDelete('cascade'); // Relation with image_id
			$table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade'); // Relation with test_id

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('samples');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}
