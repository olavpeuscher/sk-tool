<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rankings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('consult_id');
			$table->tinyInteger('type');
			$table->smallInteger('value');

			$table->foreign('consult_id')->references('id')->on('consults')->onDelete('cascade'); // Relation with consult_id
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rankings');
	}

}
