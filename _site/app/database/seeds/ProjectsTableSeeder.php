<?php

class ProjectsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Project::create(array(
        	'company_id' => '1',
        	'project_name' => 'Testproject',
        	'project_brand' => 'Knorr',
        	'project_number' => 'P0001A',
        	'project_date' => '2015-02-01'
        	)
        );
        Project::create(array(
        	'company_id' => '2',
        	'project_name' => 'Testproject2',
        	'project_brand' => 'Bolletje',
        	'project_number' => 'P0002A',
        	'project_date' => '2015-03-02'
        	)
        );
        Project::create(array(
                'company_id' => '3',
                'project_name' => 'Testproject3',
                'project_brand' => 'De Ruijter',
                'project_number' => 'P0003A',
                'project_date' => '2015-03-02'
                )
        );
	}

}
