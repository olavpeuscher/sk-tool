<?php

class ConsultsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Consult::create(array(
        	'test_id' => '1',
        	'consult_name' => 'testconsult',
        	'consult_mod_name' => 'Moderator',
        	'consult_comment' => 'Consult comment',
        	'consult_comment_brand' => 'Consult comment brand',
        	'consult_comment_targetgroup' => 'Consult comment target group',
        	)
        );
	}

}
