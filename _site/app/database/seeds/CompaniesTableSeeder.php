<?php

class CompaniesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Company::create(array(
        	'company_name' => 'Unilever'
        	)
        );
        Company::create(array(
        	'company_name' => 'Bolletje N.V.'
        	)
        );
        Company::create(array(
        	'company_name' => 'De Ruijter'
        	)
        );
	}

}
