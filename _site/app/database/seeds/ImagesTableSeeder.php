<?php

class ImagesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    	Image::create(
    		array(
    			'image_url' => 'http://lorempixel.com/400/200/nightlife/'
    		)
    	);
    	Image::create(
    		array(
    			'image_url' => 'http://lorempixel.com/400/200/people/'
    		)
    	);
    	Image::create(
    		array(
    			'image_url' => 'http://lorempixel.com/400/200/nature/'
    		)
    	);
    	Image::create(
    		array(
    			'image_url' => 'http://lorempixel.com/400/200/sports/'
    		)
    	);
    	
	}

}
