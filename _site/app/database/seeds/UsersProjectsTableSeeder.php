<?php

class UsersProjectsTableSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        foreach (range(1,3) as $project_id) {
            DB::table('users_projects')->insert(
                [
                    'user_id' => 1,
                    'project_id' => $project_id,
                ]
            );
        }
    }

}
