<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
		$this->call('CompaniesTableSeeder');
		$this->call('ImagesTableSeeder');

		$this->call('ProjectsTableSeeder');
		$this->call('TestsTableSeeder');
		$this->call('ConsultsTableSeeder');
		
		$this->call('SamplesTableSeeder');
		$this->call('RankingsTableSeeder');
		$this->call('UsersProjectsTableSeeder');
		
	}

}
