<?php

class TestsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Test::create(array(
        	'project_id' => '1',
        	'test_name' => 'Eerste test',
        	'test_mod_name' => 'Moderator Test',
        	'test_product' => 'Knorr Gehaktballetjes'
        	)
        );
        Test::create(array(
        	'project_id' => '1',
        	'test_name' => 'Tweede test',
        	'test_mod_name' => 'Moderator Test',
        	'test_product' => 'Knorr Gehaktballetjes tweede test'
        	)
        );
	}

}
