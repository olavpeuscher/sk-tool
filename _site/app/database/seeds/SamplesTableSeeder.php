<?php

class SamplesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    	Sample::create(
    		array(
    			'image_id' => '1',
    			'sample_name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	Sample::create(
    		array(
    			'image_id' => '2',
    			'sample_name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	Sample::create(
    		array(
    			'image_id' => '3',
    			'sample_name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	Sample::create(
    		array(
    			'image_id' => '4',
    			'sample_name' => 'testsample',
    			'test_id' => '1'
    		)
    	);
    	
	}

}
