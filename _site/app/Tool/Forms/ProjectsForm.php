<?php namespace Tool\Forms;

use Laracasts\Validation\FormValidator;

class ProjectsForm extends FormValidator {

	/**
	* Validation rules for new project form
	* @var array
	*/

	protected $rules = [
		'company_id' => 'required',
		'project_name' => 'required|unique:projects',
		'project_brand' => 'required',
		'project_number' => 'required',
		'project_date' => ''
	];
}