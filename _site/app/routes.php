<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
* Route the homepage
*/

Route::get('/', [
	'as' => 'home',
	'uses' => 'PagesController@home'
]);

/*
* Route tho a projects
*/
Route::get('/projects', [
	'as' => 'project_path',
	'uses' => 'ProjectsController@index'
]);

/*
* Route tho a projects
*/
Route::get('/projects/{id}', [
	'as' => 'project_show',
	'uses' => 'ProjectsController@show'
]);


Route::get('/projects/new', [
	'as' => 'project_new_path',
	'uses' => 'ProjectsController@create'
]);

Route::post('/projects/new', [
	'as' => 'project_new_path',
	'uses' => 'ProjectsController@store'
]);

/*
* Register a new user
*/

Route::get('register', [
	'as' => 'register_path',
	'uses' => 'RegistrationController@create'
]);

Route::post('register',[
	'as' => 'register_path',
	'uses' => 'RegistrationController@store'
]);

/*
* Sessions
*/

Route::get('login', [
	'as' => 'login_path',
	'uses' => 'SessionsController@create'
]);

Route::post('login', [
	'as' => 'login_path',
	'uses' => 'SessionsController@store'
]);

Route::get('logout', array('as' => 'logout_path', function () {
    Auth::logout();
    return Redirect::route('home');
	}))->before('auth');
