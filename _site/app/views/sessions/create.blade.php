@extends('layouts.default')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xs-12">
				<h2>Log in</h2>

				@if($errors->any())
				
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error}}</li>
						@endforeach
					</ul>
				</div>

				@endif

				{{ Form::open(['route' => 'login_path']) }}

				<!-- Email form input -->
					<div class="form-group">
						{{ Form::label('email', 'Email:') }}
						{{ Form::text('email', null, ['class' => 'form-control']) }}
					</div>

					<!-- Password form input -->
					<div class="form-group">
						{{ Form::label('password', 'Password:') }}
						{{ Form::password('password', ['class' => 'form-control']) }}
					</div>

					<!-- Submit buttom -->
					<div class="form-group">
						{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
					</div>
				
				{{ Form::close() }}

			</div>
		</div>
	</div>
@stop

