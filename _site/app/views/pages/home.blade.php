@extends('layouts.default')

@section('content')
	<div class="container">
		<h3>Welcome to SK tool development</h3>

		{{ link_to_route('project_path', 'Projects', null, array('class' => 'btn btn-default')) }}
	</div>
@stop