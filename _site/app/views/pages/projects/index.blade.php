@extends('layouts.default')

@section('content')
	<div class="container">

        <h3>Mijn projecten</h3>

        {{ link_to_route('project_new_path', 'New project', null, array('class' => 'btn btn-default')) }}
        <ul>
        @foreach ($projects as $project)
        	<li>{{ link_to_route('project_show', $project->project_name, ['id' => $project->id], null) }}</li>
		@endforeach
        </ul>
	</div>
@stop