@extends('layouts.default')

@section('content')
	
	<div class="container">
		<div class="col-lg-6 col-xs-12">
			<h2>Nieuw project</h2>

			@if($errors->any())
					
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error}}</li>
						@endforeach
					</ul>
				</div>

			@endif

			{{ Form::open(['route' => 'project_new_path']) }}

				<!-- Project name form input -->
				<div class="form-group">
					{{ Form::label('company_id', 'Company:') }}
					{{ Form::input('number', 'company_id', null, array('class' => 'form-control')) }}  
				</div>

				<!-- Project name form input -->
				<div class="form-group">
					{{ Form::label('project_name', 'Project name:') }}
					{{ Form::text('project_name', null, ['class' => 'form-control']) }}
				</div>

				<!-- project brand form input -->
				<div class="form-group">
					{{ Form::label('project_brand', 'Project brand:') }}
					{{ Form::text('project_brand', null, ['class' => 'form-control']) }}
				</div>

				<!-- project number form input -->
				<div class="form-group">
					{{ Form::label('project_number', 'Project number:') }}
					{{ Form::text('project_number', null, ['class' => 'form-control']) }}
				</div>

				<!-- project start date form input -->
				<div class="form-group">
					{{ Form::label('project_date', 'Project start date:') }}
					{{ Form::input('date', 'project_date', '', array('class' => 'form-control')) }}
				</div>

				
				<!-- Submit buttom -->
				<div class="form-group">
					{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
				</div>
			
			{{ Form::close() }}
		</div>

	</div>
@stop