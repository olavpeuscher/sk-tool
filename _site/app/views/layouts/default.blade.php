<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	
	<title>SK Development</title>

	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/main.css">

</head>
<body>

	<div class="wrapper">

		@include('layouts.partials.nav')
		
		@yield('content')

	</div>
	
</body>
</html>