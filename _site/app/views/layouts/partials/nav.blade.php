<header class="navbar navbar-static-top bg-gray" role="banner">
  <div class="container">
    <div class="navbar-header">
       <a href="/" class="sk-logo"></a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
      </ul>
      <ul class="nav navbar-nav navbar-right">
         @if(Auth::check())
            <li>{{ link_to_route('logout_path', 'Logout ('.Auth::user()->username.')') }}</li>
          @else
            <li>{{ link_to_route('register_path', 'Sign up', null, null) }}</li>
            <li>{{ link_to_route('login_path', 'Sign in', null, null) }}</li>
          @endif
      </ul>
    </nav>
  </div>
</header>