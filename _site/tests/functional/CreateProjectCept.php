<?php 
$I = new FunctionalTester($scenario);

$I->am('a SK tool user');
$I->wantTo('I want to login and create a new project');

$I->amOnPage('/login');
$I->fillField('email', 'foo@bar.com');
$I->fillField('password', 'test');
$I->click('Submit');

$I->seeInCurrentUrl('/projects');

$I->click('New project');

$I->amOnPage('/projects/new');

//Add company selection later

$I->fillField('company_id', '1');
$I->fillField('project_name', 'Testproject');
$I->fillField('project_brand', 'TestBrand');
$I->fillField('project_number', 'P00001');
$I->fillField('project_date', '04/04/2015');

$I->click('Submit');

$I->seeRecord('users', [
	'company_id'=> '1',
	'project_name'=> 'Testproject',
	'project_brand'=> 'TestBrand',
	'project_number'=> 'P00001',
	'project_date'=> '2015-04-04'
]);

$I->seeCurrentUrlEquals('/projects');

