<?php 
$I = new FunctionalTester($scenario);
$I->am('guest');
$I->wantTo('sign up for a SK account');

$I->amOnPage('/');
$I->click('Sign up');
$I->seeCurrentUrlEquals('/register');

$I->fillField('Username:', 'TestAccounttest');
$I->fillField('Email:', 'test@example.com');
$I->fillField('Password:', 'root');
$I->fillField('Password confirmation:', 'root');

$I->click('Submit');

$I->seeRecord('users', [
	'username' => 'TestAccounttest',
	'email' => 'test@example.com'
]);